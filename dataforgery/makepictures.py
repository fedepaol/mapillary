import shutil
import piexif
import datetime

FILES = ['first.jpg', 'second.jpg', 'third.jpg']
time = datetime.time(8, 0, 0)
date = datetime.date(2016, 8, 7)
when = datetime.datetime.combine(date, time)
delta = datetime.timedelta(seconds=10)
date_format = '%Y:%m:%d %H:%M:%S'

for i in range (0, 100):
    base = FILES[i % 3]
    new_file = 'images/picture_%d.jpg' % i
    shutil.copyfile(base, new_file)
    exif_dict = piexif.load(new_file)
    exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal] = when.strftime(date_format)

    when = when + delta
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, new_file)

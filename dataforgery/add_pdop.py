import random

f = open('activity_orig.gpx')
out = open('activity.gpx', 'w')
found_track = False
for line in f:
    if line.find('trkseg'):
        found_track = True
    if line.find('time') != -1 and found_track:
        out.write('        <pdop>%f</pdop>\n' % random.randint(0, 20) )
    out.write(line)

out.close()

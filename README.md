##Intro
The architecture of the app is consistent with what I am more comfortable with, which is Model View Presenter pattern, RxJava and Dagger 2 for dependency injection.
I think the app is good enough to show what I know (and what I do not know) about Android. 

#Notes
###Gpx parsing / image placing:
I tried to optimize the parsing of gpx / image positioning process, taking advantadge of the fact that the gpx points are already ordered. The only extra time is spent sorting the images by time. While parsing, the points are emitted in a sequence. For every point it checks if the earliest image is between the point and the previous one. If it does, the location of the nearest point is assigned to the image and the image is taken from the queue. 

The result felt a bit clunky because of some edge cases I had to deal with but is more efficient in terms of computation, however it comes with unit tests that helped me to speed the development up.
What's more, the clunkiness is hidden under the rug and the result is seen as a couple of observables that "just emit the data we need".

###UX:
The requirements were a bit blurry, so I kind of interpreted them.

* I assumed that the points in the gpx are a track, which means that they are expected to be sorted in chronological order and that there won't be points distant an hour one from each other.
* I assumed that the app will find the file it needs in the storage (getExternalFilesDir()). The images path / gpx file are a costant, but it can be easily replaced with parameters taken from an intent.
* What I wanted to do was that clicking an image highlighted the marker and viceversa. What happened was that the marker interaction is far from perfect in mapbox, so it's not really working.
* I preferred to associate the quality of the location to the single markers with green, yellow, red as opposed to the track. It looks a bit nicer than the heat map and I expect the user to want to be sure that the location of the image is right.
* **BONUS POINT**: By long clicking an image the app enters in "moving" state. Which means that the user can click on the map and the marker will be moved. Then the user can choose to undo or save the new location is saved or undone.

###Mapbox:
It's really powerful. It ate all the points / markers I gave it gracefully. However it has some rough edges made me spend more time than expected:

* Touch events go bananas more often than not. See -> [https://github.com/mapbox/mapbox-gl-native/pull/8585]
* I found an ugly bug that the markers if the location I zoom to is far from the current location. The only way I manage to deal with it was to redraw the markers. 
* When rotating the map the markers do not rotate. I disabled this since a marker pointing to east was suddenly ponting to north.


###The data:
As I said, the app expects the data to be already there under /images and as "activity.gpx" (getExternalFilesDir returns /Android/data/com.mapillary.mapillarychallenge/files on my phone) .
Having some data to test was harder than expected, so I had to do some basic python-fu to generate them.

* I found a long track from a friend who goes cycling. I had to inject random pdop values since it did not contain them
* I generated fake images starting from three different ones, changing the time inside the exif to be consistent with the data I had. It's three images instead of one so I was able to check if something was wrong.
The code to generate fake data can be found under the "dataforgery" subfolder.


###Where to go from here:
The state is represented with two member variables that are differnet from -1 if the state is "image selected" or "image moving". I would rather refactor that into different classes that handle different scenarios since it feels a bit too hodgepodgey.
Given the fact that marker interaction is unrelaiable, a workaround could be to intercept the touch on the map and figure out which marker (if any) is being touched by checking the position.
I am not sure if a good interaction would be to filter the images depending on the area shown in the map. It's something it's worth experimenting, but I am 100% sure about that.

package com.mapillary.mapillarychallenge.model;

// as per wikipidia definition (TODO link)
public enum PositionQuality {
    EXCELLENT, AVERAGE, POOR
}

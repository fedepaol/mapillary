package com.mapillary.mapillarychallenge.model;

import com.google.auto.value.AutoValue;

import java.util.Date;

/**
 * Created by fedepaol on 27/03/17.
 */

@AutoValue
public abstract class Position {
    public abstract double latitude();
    public abstract double longitude();

    public static Position create(double latitude, double longitude) {
        return new AutoValue_Position(latitude, longitude);
    }


}

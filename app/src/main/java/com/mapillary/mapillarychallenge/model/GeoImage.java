package com.mapillary.mapillarychallenge.model;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class GeoImage {
    public abstract Image image();
    public abstract Position position();
    public abstract float orientation();
    public abstract PositionQuality positionQuality();

    public static GeoImage create(Image image, Position position, float orientation, PositionQuality quality) {
        return new AutoValue_GeoImage(image, position, orientation, quality);
    }
}

package com.mapillary.mapillarychallenge.model;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

import java.util.Date;

/**
 * Created by fedepaol on 27/03/17.
 */

@AutoValue
public abstract class Image implements Comparable {
    public abstract String imagePath();
    public abstract Date imageDate();

    public static Image create(String imagePath, Date imageDate) {
        return new AutoValue_Image(imagePath, imageDate);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (o instanceof Image) {
            Image d1 = (Image) o;
            return d1.imageDate().compareTo(imageDate());
        }
        return 0;
    }
}

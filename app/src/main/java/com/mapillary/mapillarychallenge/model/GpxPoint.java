package com.mapillary.mapillarychallenge.model;

import com.google.auto.value.AutoValue;

import java.util.Date;

/**
 * Created by fedepaol on 28/03/17.
 */

@AutoValue
public abstract class GpxPoint {
    public abstract Position position();
    public abstract float accuracy();
    public abstract Date date();

    public static GpxPoint create(Position position, float accuracy, Date date) {
        return new AutoValue_GpxPoint(position, accuracy, date);
    }
}

package com.mapillary.mapillarychallenge.schedule;

import rx.Scheduler;

public interface SchedulersProvider {
    Scheduler mainThread();

    Scheduler io();

    Scheduler computation();
}

package com.mapillary.mapillarychallenge;

import android.app.Application;

import com.mapillary.mapillarychallenge.inject.ApplicationComponent;
import com.mapillary.mapillarychallenge.inject.ApplicationModule;
import com.mapillary.mapillarychallenge.inject.DaggerApplicationComponent;


public class ChallengeApplication extends Application {
    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
    }

    ApplicationModule getApplicationModule() {
        return new ApplicationModule(this);
    }

    void initComponent() {
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(getApplicationModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }
}

package com.mapillary.mapillarychallenge.internals;


import com.mapillary.mapillarychallenge.model.GeoImage;
import com.mapillary.mapillarychallenge.model.GpxPoint;
import com.mapillary.mapillarychallenge.model.Image;
import com.mapillary.mapillarychallenge.model.Position;
import com.mapillary.mapillarychallenge.model.PositionQuality;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.observables.ConnectableObservable;


public class FetchDataTask {
    private PicturesFinder mPicturesFinder;
    private GpxProcessor mGpxProcessor;

    private ConnectableObservable<GpxPoint> pointsObservable;
    private Observable<List<Image>> imagesObservable;

    public FetchDataTask(String imagesPath,
                         String gpxFilePath,
                         PicturesFinder picturesFinder,
                         GpxProcessor gpxProcessor) {
        mPicturesFinder = picturesFinder;
        mGpxProcessor = gpxProcessor;
        imagesObservable = mPicturesFinder.imageDataObservable(imagesPath);
        pointsObservable = mGpxProcessor.gpxParserObservable(gpxFilePath).publish();
    }

    public Observable<GpxPoint> pointsObservable() {
        return pointsObservable;
    }

    public Observable<List<GeoImage>> geoImageObservable() {
        return imagesObservable
                .flatMap(this::imagesFromPoints);
    }

    public void startEmitting() {
        pointsObservable.connect();
    }

    private Observable<List<GeoImage>> imagesFromPoints(List<Image> images) {
        Observable<List<GeoImage>> res =
                pointsObservable
                        .buffer(2, 1)       // every time takes the latest two elements
                        .filter(pair -> pair.size() == 2)
                        .flatMap(points -> consumeImagesBetweenPoints(images, points))
                        .filter(image -> image != null).toList();

        return res;
    }

    /**
     * Processes and removes all the images from an ordered list that are between the two given points
     * @param images
     * @param points
     * @return
     */
    private Observable<GeoImage> consumeImagesBetweenPoints(List<Image> images, List<GpxPoint> points) {
        if (images.size() == 0) {
            return Observable.empty();
        }

        GpxPoint first = points.get(0);
        GpxPoint second = points.get(1);

        emptyEarlierImages(images, first.date()); // if the image time is earlier than the first, it won't be
                                                  // between any of the points


        if (images.size() == 0) {
            return Observable.empty();
        }

        List<GeoImage> imagesBetweenPoints = new LinkedList<>();
        Image firstImage = images.get(0);

        // has to be a while since there can be more than one image between two points
        while (firstImage.imageDate().compareTo(first.date()) >= 0 &&
                firstImage.imageDate().compareTo(second.date()) < 0) {


            GeoImage geoImage = GeoImage.create(firstImage,
                                                imagePosition(firstImage, first, second),
                                                calcOrientation(first.position(), second.position()),
                                                imagePositionQuality(firstImage, first, second));
            imagesBetweenPoints.add(geoImage);
            images.remove(0);
            if (images.size() == 0) {
                break;
            }
            firstImage = images.get(0);
        }
        return Observable.from(imagesBetweenPoints);

    }

    private void emptyEarlierImages(List<Image> images, Date upperBound) {
        while (images.size() > 0 && images.get(0).imageDate().compareTo(upperBound) < 0) {
            images.remove(0);
        }
    }

    /*
        returns the location of the nearest point in terms of temporal distance. This works if the
        points are dense enough, otherwise we could take the middle point
     */
    private Position imagePosition(Image image, GpxPoint first, GpxPoint second) {
        GpxPoint nearest =getNearestPointToImage(image, first, second);
        return nearest.position();
    }

    private PositionQuality imagePositionQuality(Image image, GpxPoint p1, GpxPoint p2) {
        GpxPoint nearest = getNearestPointToImage(image, p1, p2);
        float accuracy = nearest.accuracy();
        if (accuracy <= 2) {
            return PositionQuality.EXCELLENT;
        }
        if (accuracy > 20) {
            return PositionQuality.POOR;
        }
        return PositionQuality.AVERAGE;
    }

    private GpxPoint getNearestPointToImage(Image image, GpxPoint first, GpxPoint second) {
        long firstElapsed = image.imageDate().getTime() - first.date().getTime();
        long secondElapsed = second.date().getTime() - image.imageDate().getTime();

        if (firstElapsed < secondElapsed) {
            return first;
        }
        return second;
    }

    // Taken trustfully from the Internet
    private float calcOrientation(Position p1, Position p2) {
        double dLon = (p2.longitude() - p1.longitude());

        double y = Math.sin(dLon) * Math.cos(p2.latitude());
        double x = Math.cos(p1.latitude()) * Math.sin(p2.latitude()) - Math.sin(p1.latitude())
                * Math.cos(p2.latitude()) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

        return (float) brng;
    }
}

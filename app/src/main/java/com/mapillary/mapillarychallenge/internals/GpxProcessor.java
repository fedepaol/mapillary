package com.mapillary.mapillarychallenge.internals;

import android.util.Xml;

import com.mapillary.mapillarychallenge.model.GpxPoint;
import com.mapillary.mapillarychallenge.model.Position;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import rx.AsyncEmitter;
import rx.Observable;

/**
 * Created by fedepaol on 28/03/17.
 */

public class GpxProcessor {
    private static final String ns = null;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // TODO Locale

    @Inject
    public GpxProcessor() {

    }

    public Observable<GpxPoint> gpxParserObservable(final String fileName) {
        return Observable.fromAsync(gpxPointEmitter -> {
            try {
                InputStream in = new FileInputStream(fileName);
                parse(in, gpxPointEmitter);
                in.close();
                gpxPointEmitter.onCompleted();
            } catch (IOException e) {
                gpxPointEmitter.onError(e);
            }
        }, AsyncEmitter.BackpressureMode.BUFFER);
    }

    private void parse(InputStream in, AsyncEmitter<GpxPoint> emitter) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            readFile(parser, emitter);
        } catch (Exception e) {
            emitter.onError(e);
        }
    }

    private void readFile(XmlPullParser parser, AsyncEmitter<GpxPoint> emitter) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, ns, "gpx");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("trk")) {
                readTrack(parser, emitter);
            } else {
                skip(parser);
            }
        }
    }

    private void readTrack(XmlPullParser parser, AsyncEmitter<GpxPoint> emitter) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, ns, "trk");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("trkseg")) {
                readSegment(parser, emitter);
            }  else {
                skip(parser);
            }
        }
    }

    private void readSegment(XmlPullParser parser, AsyncEmitter<GpxPoint> emitter) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, ns, "trkseg");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("trkpt")) {
                readPoint(parser, emitter);
            } else {
                skip(parser);
            }
        }
    }

    private void readPoint(XmlPullParser parser, AsyncEmitter<GpxPoint> emitter) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, ns, "trkpt");
        double latitude = Double.valueOf(parser.getAttributeValue(null, "lat"));
        double longitude = Double.valueOf(parser.getAttributeValue(null, "lon"));
        float accuracy = 0;
        Date date = new Date();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("time")) {
                date = mDateFormat.parse(readText(parser));
            } else if (name.equals("pdop")) {
                accuracy = Float.valueOf(readText(parser));
            } else {
                skip(parser);
            }

        }
        GpxPoint point = GpxPoint.create(Position.create(latitude, longitude), accuracy, date);
        emitter.onNext(point);
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}

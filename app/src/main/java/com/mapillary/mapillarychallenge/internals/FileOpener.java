package com.mapillary.mapillarychallenge.internals;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by fedepaol on 28/03/17.
 */

public class FileOpener {
    private Context mContext;

    @Inject
    public FileOpener(Context c) {
        mContext = c;
    }

    public Observable<InputStream> getFileObservable(String fileName) {
        return Observable.fromCallable(() -> openFile(fileName));
    }

    private InputStream openFile(String fileName) throws IOException {
        return mContext.getAssets().open(fileName);
    }
}

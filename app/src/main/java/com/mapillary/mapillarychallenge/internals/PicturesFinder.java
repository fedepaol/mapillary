package com.mapillary.mapillarychallenge.internals;

import android.content.Context;

import com.mapillary.mapillarychallenge.model.Image;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import rx.AsyncEmitter;
import rx.Observable;

/**
 * Created by fedepaol on 28/03/17.
 */

public class PicturesFinder {
    private ExifReader mExifReader;

    @Inject
    public PicturesFinder(ExifReader exifReader) {
        mExifReader = exifReader;
    }

    public Observable<List<Image>> imageDataObservable(String path) {
        return imagesInPath(path)
                .filter(this::isPicture)
                .map(file -> {
                    String fullName = path + "/" + file;
                    return Image.create(fullName, getPictureDate(fullName));
                })
                .filter(i -> i.imageDate() != null)
                .toSortedList((first, second) -> first.imageDate().compareTo(second.imageDate()));
    }

    private Observable<String> imagesInPath(String path) {
        return Observable.fromAsync(fileEmitter -> {
            File directory = new File(path);
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    String fileName = file.getName();
                    fileEmitter.onNext(fileName);
                }
            }
            fileEmitter.onCompleted();
        }, AsyncEmitter.BackpressureMode.BUFFER);
    }

    private Date getPictureDate(String fullName) {
        try {
            return mExifReader.getDateFromImage(fullName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isPicture(String name) {
        String filenameArray[] = name.split("\\.");
        String extension = filenameArray[filenameArray.length - 1].toLowerCase();
        return extension.equals("jpg") || extension.equals("jpeg");
    }

}

package com.mapillary.mapillarychallenge.internals;

import android.support.media.ExifInterface;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by fedepaol on 29/03/17.
 */

public class ExifReader {
    public Date getDateFromImage(String imagePath) throws IOException {
        InputStream is = new FileInputStream(imagePath);

        ExifInterface exifInterface = new ExifInterface(is);
        Date res = new Date(exifInterface.getDateTime());
        is.close();
        return res;
    }
}

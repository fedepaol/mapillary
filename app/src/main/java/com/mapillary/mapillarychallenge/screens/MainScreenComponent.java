package com.mapillary.mapillarychallenge.screens;


import com.mapillary.mapillarychallenge.inject.ActivityScope;
import com.mapillary.mapillarychallenge.inject.ApplicationComponent;

import dagger.Component;

@ActivityScope
@Component(modules = {MainScreenModule.class},
           dependencies = {ApplicationComponent.class})
public interface MainScreenComponent {
    void inject(ChallengeActivity activity);
}


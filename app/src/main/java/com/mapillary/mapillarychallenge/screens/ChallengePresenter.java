package com.mapillary.mapillarychallenge.screens;

import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by fedepaol on 23/03/17.
 */

public interface ChallengePresenter {
    void onViewAttached(ChallengeView view);
    void onViewDetached();
    boolean onBackPressed();
    void onMapLocationClicked(LatLng newLocation);
    void onSaveMovedImagePosition();
    void onRestoreMovedImagePosition();
}



package com.mapillary.mapillarychallenge.screens;


import com.mapillary.mapillarychallenge.internals.GpxProcessor;
import com.mapillary.mapillarychallenge.internals.PicturesFinder;
import com.mapillary.mapillarychallenge.schedule.SchedulersProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class MainScreenModule {
    private ChallengeView mView;
    public MainScreenModule(ChallengeView view) {
        mView = view;
    }

    @Provides
    public ChallengeView provideMainView() {
        return mView;
    }

    @Provides
    public ChallengePresenter provideMainPresenter(GpxProcessor processor,
                                                   PicturesFinder picturesFinder,
                                                   SchedulersProvider schedulers){
        return new ChallengePresenterImpl(processor, picturesFinder, schedulers);
    }


}

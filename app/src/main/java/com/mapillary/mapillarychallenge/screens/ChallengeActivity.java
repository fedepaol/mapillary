package com.mapillary.mapillarychallenge.screens;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jakewharton.rxrelay.PublishRelay;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerView;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapillary.mapillarychallenge.ChallengeApplication;
import com.mapillary.mapillarychallenge.R;
import com.mapillary.mapillarychallenge.model.GeoImage;
import com.mapillary.mapillarychallenge.model.PositionQuality;
import com.mapillary.mapillarychallenge.utils.TwoWayHashmap;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ChallengeActivity extends AppCompatActivity implements OnMapReadyCallback, ChallengeView, MapboxMap.OnMarkerClickListener, MapboxMap.OnMapClickListener {
    private static final String IMAGES_PATH = "/images"; // could be passed in the intent
    private static final String GPX_FILE = "/activity.gpx"; // could be passed in the intent

    @Bind(R.id.mapView)
    MapView mMapView;

    @Bind(R.id.images)
    RecyclerView mImagesList;

    @Bind(R.id.pending_request_progress)
    ProgressBar mProgress;

    @Bind(R.id.marker_move_controls)
    LinearLayout mMarkerControls;

    @Inject
    ChallengePresenter mPresenter;

    private MapboxMap mMap;
    private ImagesAdapter mImagesAdapter;
    private Icon mBadMarkerIcon;
    private Icon mAvgMarkerIcon;
    private Icon mGoodMarkerIcon;
    private Icon mSelectedMarkerIcon;
    private TwoWayHashmap<Integer, Marker> mMarkerImageMap;
    private PublishRelay<Integer> mMarkerForImageObservable;
    private Icon mMovingMarkerIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1IjoiZmVkZXBhb2wiLCJhIjoiY2owcXcxcDFqMDJkejJ4bnBpOXlna2g0ZSJ9.z00DY2j0seK7BhRvj5Vhig");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mMapView.onCreate(savedInstanceState);
        ChallengeApplication app = (ChallengeApplication) getApplication();
        DaggerMainScreenComponent.builder()
                .applicationComponent(app.getComponent())
                .mainScreenModule(new MainScreenModule(this))
                .build().inject(this);

        mImagesList.setLayoutManager(new GridLayoutManager(this, 3));
        mImagesAdapter = new ImagesAdapter(getApplicationContext());
        mImagesList.setAdapter(mImagesAdapter);

        mMarkerImageMap = new TwoWayHashmap<>();
        mMarkerForImageObservable = PublishRelay.create();
        showLoading(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
        if (mMap != null) {
            mPresenter.onViewAttached(this);
        } else {
            mMapView.getMapAsync(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onViewDetached();
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMap = mapboxMap;
        disableRotation();

        IconFactory iconFactory = IconFactory.getInstance(ChallengeActivity.this);
        mBadMarkerIcon = iconFactory.fromResource(R.drawable.marker_bad_signal);
        mAvgMarkerIcon = iconFactory.fromResource(R.drawable.marker_decent_signal);
        mGoodMarkerIcon = iconFactory.fromResource(R.drawable.marker_good_signal);
        mSelectedMarkerIcon = iconFactory.fromResource(R.drawable.marker_selected);
        mMovingMarkerIcon = iconFactory.fromResource(R.drawable.marker_moving);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        mPresenter.onViewAttached(this);
    }

    @Override
    public void showLoading(boolean loading) {
        if (loading) {
            mProgress.setVisibility(VISIBLE);
        } else {
            mProgress.setVisibility(GONE);
        }
    }

    @Override
    public void displayPath(List<LatLng> points) {
        mMap.addPolyline(new PolylineOptions()
                .addAll(points)
                .color(Color.parseColor("#3bb2d0"))
                .width(2));
    }

    @Override
    public void displayMarkers(List<GeoImage> images) {

        for (int i = 0; i < images.size(); i++) {
            GeoImage image = images.get(i);

            MarkerView view = mMap.addMarker(new MarkerViewOptions()
                    .position(new LatLng(image.position().latitude(), image.position().longitude()))
                    .rotation(image.orientation())
                    .icon(iconForPositionQuality(image.positionQuality())));
            mMarkerImageMap.add(i, view);
        }

    }

    @Override
    public void displayImages(List<GeoImage> images) {
        mImagesAdapter.setData(images);
    }

    @Override
    public void highLightImage(int position, ImageSelectReason reason) {
        mImagesAdapter.unsetItemSelected();
        mImagesAdapter.setImageSelected(position, reason);
    }

    @Override
    public void unhighlightImage() {
        mImagesAdapter.unsetItemSelected();
    }

    @Override
    public void highLightMarker(int position, ImageSelectReason reason) {
        Marker marker = mMarkerImageMap.getForward(position);
        switch (reason) {
            case SELECT:
                marker.setIcon(mSelectedMarkerIcon);
                break;
            case MOVE:
                marker.setIcon(mMovingMarkerIcon);
                break;
        }
    }

    @Override
    public void unhighLightMarker(int position, GeoImage image) {
        Marker marker = mMarkerImageMap.getForward(position);
        marker.setIcon(iconForPositionQuality(image.positionQuality()));
    }

    @Override
    public void scrollImagesToImage(int position) {
        mImagesList.smoothScrollToPosition(position);
    }

    @Override
    public void centerCamera(LatLng center) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 17));
        // as per https://github.com/mapbox/mapbox-gl-native/issues/7782
    }

    @Override
    public void centerCameraToImage(LatLng center, int invalidateFrom, int invalidateTo) {
        // as per https://github.com/mapbox/mapbox-gl-native/issues/7782
        // they say it's fixed but it's not working.
        // this is ugly ugly ugly
        // I redraw up to 10 markers before the image and 10 markers after the image since
        // zooming the camera hides the markers until some interaction with the map is done
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 17),
                new MapboxMap.CancelableCallback() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onFinish() {
                        for (int j = invalidateFrom; j < invalidateTo; j++) {
                            MarkerView m = (MarkerView) mMarkerImageMap.getForward(j);
                            mMarkerImageMap.remove(j, m);
                            mMap.removeMarker(m);
                            MarkerView newMarker = mMap.addMarker(new MarkerViewOptions()
                                                .position(m.getPosition())
                                                .rotation(m.getRotation())
                                                .icon(m.getIcon()));
                            mMarkerImageMap.add(j, newMarker);
                        }

                    }
                });
    }

    @Override
    public void moveMarkerForImage(LatLng newLocation, int imageToMove) {
        MarkerView m = (MarkerView) mMarkerImageMap.getForward(imageToMove);
        m.setPosition(newLocation);
    }

    @Override
    public void onBackPressed() {
        if (!mPresenter.onBackPressed())
            super.onBackPressed();
    }

    @OnClick(R.id.button_confirm_moving)
    public void onMoveConfirmPressed() {
        mPresenter.onSaveMovedImagePosition();
    }

    @OnClick(R.id.button_delete_moving)
    public void onMoveUndoPressed() {
        mPresenter.onRestoreMovedImagePosition();
    }

    private void disableRotation() {
        UiSettings settings = mMap.getUiSettings();
        settings.setRotateGesturesEnabled(false);
    }

    @Override
    public String getImagesPath() {
        return getExternalFilesDir(null) + IMAGES_PATH;
    }

    @Override
    public String getGpxFile() {
        return getExternalFilesDir(null) + GPX_FILE;
    }

    @Override
    public Observable<Integer> imageSelectedObservable() {
        return mImagesAdapter.imageSelectedObservable();
    }

    @Override
    public Observable<Integer> imageSelectedForMovingObservable() {
        return mImagesAdapter.imageLongPressedObservable();
    }

    @Override
    public Observable<Integer> markerForImageSelectedObservable() {
        return mMarkerForImageObservable;
    }

    @Override
    public void displayMovedMarkerButtons() {
        mMarkerControls.setVisibility(VISIBLE);
    }

    @Override
    public void hideMoveMarkerButtons() {
        mMarkerControls.setVisibility(GONE);
    }

    private Icon iconForPositionQuality(PositionQuality quality) {
        switch (quality) {
            case AVERAGE:
                return mAvgMarkerIcon;
            case EXCELLENT:
                return mGoodMarkerIcon;
            case POOR:
                return mBadMarkerIcon;
        }
        return mAvgMarkerIcon;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        int imagePosition = mMarkerImageMap.getBackward(marker);
        mMarkerForImageObservable.call(imagePosition);
        return true;
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {
        mPresenter.onMapLocationClicked(point);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

package com.mapillary.mapillarychallenge.screens;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jakewharton.rxrelay.PublishRelay;
import com.mapillary.mapillarychallenge.R;
import com.mapillary.mapillarychallenge.model.GeoImage;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;

import static com.mapillary.mapillarychallenge.R.color.white;


public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageHolder> {
    private List<GeoImage> mData;
    private Context mContext;
    private int mSelectedImage = -1;
    private int mMovingImage = -1;
    PublishRelay<Integer> imageClickedRelay;
    PublishRelay<Integer> imageLongPressRelay;

    public ImagesAdapter(Context c) {
        mData = new ArrayList<>(0);
        mContext = c;
        imageClickedRelay = PublishRelay.create();
        imageLongPressRelay = PublishRelay.create();
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View el = LayoutInflater.from(parent.getContext()).inflate(R.layout.images_elem, parent, false);
        return new ImageHolder(el);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        GeoImage m = mData.get(position);
        Glide.with(mContext)
                .load("file://" + m.image().imagePath())
                .centerCrop()
                .into(holder.mImage);
        if (mSelectedImage == position) {
            holder.mFrame.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        } else if (mMovingImage == position) {
            holder.mFrame.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        } else {
            holder.mFrame.setBackgroundColor(mContext.getResources().getColor(white));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ImageHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.elem_image)
        ImageView mImage;
        @Bind(R.id.elem_frame)
        FrameLayout mFrame;

        public ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> imageClickedRelay.call(getAdapterPosition()));
            itemView.setOnLongClickListener(v -> {
                imageLongPressRelay.call(getAdapterPosition());
                return true;
            });
        }
    }

    public void setData(List<GeoImage> images) {
        mData = images;
        notifyDataSetChanged();
    }

    public Observable<Integer> imageSelectedObservable() {
        return imageClickedRelay;
    }

    public Observable<Integer> imageLongPressedObservable() {
        return imageLongPressRelay;
    }

    public void setImageSelected(int imageIndex, ChallengeView.ImageSelectReason reason) {
        switch (reason) {
            case SELECT:
                mSelectedImage = imageIndex;
            break;
            case MOVE:
                mMovingImage = imageIndex;
            break;
        }
        notifyItemChanged(imageIndex);
    }

    public void unsetItemSelected() {
        if (mSelectedImage != -1) {
            int oldSelected = mSelectedImage;
            notifyItemChanged(oldSelected);
            mSelectedImage = -1;
        }

        if (mMovingImage != -1) {
            int oldSelected = mMovingImage;
            notifyItemChanged(oldSelected);
            mMovingImage = -1;
        }
    }
}

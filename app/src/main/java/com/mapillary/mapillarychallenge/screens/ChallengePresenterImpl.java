package com.mapillary.mapillarychallenge.screens;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapillary.mapillarychallenge.internals.FetchDataTask;
import com.mapillary.mapillarychallenge.internals.GpxProcessor;
import com.mapillary.mapillarychallenge.internals.PicturesFinder;
import com.mapillary.mapillarychallenge.model.GeoImage;
import com.mapillary.mapillarychallenge.model.Position;
import com.mapillary.mapillarychallenge.schedule.SchedulersProvider;

import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.mapillary.mapillarychallenge.screens.ChallengeView.ImageSelectReason.MOVE;
import static com.mapillary.mapillarychallenge.screens.ChallengeView.ImageSelectReason.SELECT;

/**
 * Created by fedepaol on 23/03/17.
 */

public class ChallengePresenterImpl implements ChallengePresenter {
    private ChallengeView mView;
    FetchDataTask mDataTask;
    private GpxProcessor mGpxProcessor;
    private PicturesFinder mPicturesFinder;
    private CompositeSubscription mSubscription;
    private SchedulersProvider mSchedulers;
    int mSelectedImage = -1;
    int mMovingImage = -1;
    Position candidateNewLocation;

    List<GeoImage> mImages;

    public ChallengePresenterImpl(GpxProcessor gpxProcessor,
                                  PicturesFinder picturesFinder,
                                  SchedulersProvider schedulers) {
        mGpxProcessor = gpxProcessor;
        mPicturesFinder = picturesFinder;
        mSchedulers = schedulers;
    }

    @Override
    public void onViewAttached(ChallengeView view) {
        mSubscription = new CompositeSubscription();

        mView = view;
        mView.hideMoveMarkerButtons();
        mDataTask = new FetchDataTask(  view.getImagesPath(),
                                        view.getGpxFile(),
                                        mPicturesFinder,
                                        mGpxProcessor);

        Subscription s = mDataTask.pointsObservable()
                                  .map(point -> new LatLng(point.position().latitude(),
                                                           point.position().longitude()))
                                  .toList()
                                  .subscribeOn(mSchedulers.io())
                                  .observeOn(mSchedulers.mainThread())
                                  .subscribe(points -> {
                                      mView.displayPath(points);
                                      mView.centerCamera(points.get(0));
                                  },
                                          e -> {
                                              mView.showError(e.getMessage());
                                              mView.showLoading(false);
                                          }
                                  );

        Subscription s1 = mDataTask.geoImageObservable()
                                    .subscribeOn(mSchedulers.io())
                                    .observeOn(mSchedulers.mainThread())
                                    .subscribe(images -> {
                                        mImages = images;
                                        mView.displayImages(images);
                                        mView.displayMarkers(images);
                                        mView.showLoading(false);
                                    },
                                            e -> {
                                                mView.showError(e.getMessage());
                                                mView.showLoading(false);
                                            });

        mDataTask.startEmitting();

        Subscription s2 = mView.imageSelectedObservable()
                               .subscribeOn(mSchedulers.mainThread())
                               .observeOn(mSchedulers.mainThread())
                               .subscribe(this::onImageSelected);

        Subscription s3 = mView.markerForImageSelectedObservable()
                                    .subscribeOn(mSchedulers.mainThread())
                                    .observeOn(mSchedulers.mainThread())
                                    .subscribe(this::onImageSelected);

        Subscription s4 = mView.imageSelectedForMovingObservable()
                .subscribeOn(mSchedulers.mainThread())
                .observeOn(mSchedulers.mainThread())
                .subscribe(this::onImageSelectedForMoving);



        mSubscription.add(s);
        mSubscription.add(s1);
        mSubscription.add(s2);
        mSubscription.add(s3);
        mSubscription.add(s4);

    }

    private void onImageSelected(int imageId) {
        if (imageId == mSelectedImage || imageId == mMovingImage) {
            unHighlightSelectedImage();
        } else {
            unHighlightSelectedImage();
            mSelectedImage = imageId;
            mView.highLightImage(imageId, SELECT);
            mView.highLightMarker(imageId, SELECT);
            centerCameraToImage(imageId);
            mView.scrollImagesToImage(imageId);
        }
    }

    private void onImageSelectedForMoving(int imageId) {
        if (imageId == mMovingImage) {
            return;
        }
        unHighlightSelectedImage();
        mMovingImage = imageId;
        candidateNewLocation = mImages.get(imageId).position();
        mView.highLightImage(imageId, MOVE);
        mView.highLightMarker(imageId, MOVE);
        centerCameraToImage(imageId);
        mView.displayMovedMarkerButtons();
    }

    private void centerCameraToImage(int imageId) {
        GeoImage image = mImages.get(imageId);
        mView.centerCameraToImage(new LatLng(image.position().latitude(),
                        image.position().longitude()),
                Math.max(0, imageId - 10),
                Math.min(mImages.size() - 1, imageId + 10));
    }

    private void unHighlightSelectedImage() {
        if (mSelectedImage != -1) {
            mView.unhighlightImage();
            mView.unhighLightMarker(mSelectedImage, mImages.get(mSelectedImage));
            mSelectedImage = -1;
        }
        if (mMovingImage != -1) {
            mView.unhighlightImage();
            mView.unhighLightMarker(mMovingImage, mImages.get(mMovingImage));
            mMovingImage = -1;
        }
        mView.hideMoveMarkerButtons();
    }

    @Override
    public void onMapLocationClicked(LatLng newLocation) {
        if (mMovingImage == -1) {
            return;
        }
        candidateNewLocation = Position.create(newLocation.getLatitude(), newLocation.getLongitude());
        mView.moveMarkerForImage(newLocation, mMovingImage);
    }

    @Override
    public void onSaveMovedImagePosition() {
        GeoImage movedImage = mImages.get(mMovingImage);
        GeoImage newImage = GeoImage.create(movedImage.image(),
                                            candidateNewLocation,
                                            movedImage.orientation(),
                                            movedImage.positionQuality());
        mImages.remove(mMovingImage);
        mImages.add(mMovingImage, newImage);
        unHighlightSelectedImage();
    }

    @Override
    public void onRestoreMovedImagePosition() {
        GeoImage movingImage = mImages.get(mMovingImage);
        // restoring back to the original position
        mView.moveMarkerForImage(positionToLatLng(movingImage.position()), mMovingImage);
        unHighlightSelectedImage();
    }

    @Override
    public void onViewDetached() {
        mView = null;
        mSubscription.unsubscribe();
    }

    @Override
    public boolean onBackPressed() {
        if (mSelectedImage != -1 || mMovingImage != -1) {
            unHighlightSelectedImage();
            return true;
        }
        return false;
    }

    private LatLng positionToLatLng(Position p) {
        return new LatLng(p.latitude(), p.longitude());
    }
}

package com.mapillary.mapillarychallenge.screens;

import android.location.Location;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapillary.mapillarychallenge.model.GeoImage;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by fedepaol on 23/03/17.
 */

public interface ChallengeView {
    enum ImageSelectReason {
        SELECT,
        MOVE;
    }
    void showLoading(boolean loading);

    void displayPath(List<LatLng> points);

    void centerCamera(LatLng center);

    String getImagesPath();

    String getGpxFile();

    void displayMarkers(List<GeoImage> images);

    void displayImages(List<GeoImage> images);

    Observable<Integer> imageSelectedObservable();

    Observable<Integer> imageSelectedForMovingObservable();

    Observable<Integer> markerForImageSelectedObservable();

    void highLightImage(int position, ImageSelectReason reason);

    void unhighlightImage();

    void highLightMarker(int position, ImageSelectReason reason);

    void unhighLightMarker(int position, GeoImage image);

    void scrollImagesToImage(int position);

    void centerCameraToImage(LatLng latLng, int from, int to);

    void displayMovedMarkerButtons();

    void hideMoveMarkerButtons();

    void moveMarkerForImage(LatLng newLocation, int mMovingImage);

    void showError(String message);
}

package com.mapillary.mapillarychallenge.inject;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.github.pwittchen.reactivesensors.library.ReactiveSensors;
import com.google.gson.Gson;
import com.mapillary.mapillarychallenge.internals.ExifReader;
import com.mapillary.mapillarychallenge.schedule.RealSchedulersProvider;
import com.mapillary.mapillarychallenge.schedule.SchedulersProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {
    private Application mApp;

    public ApplicationModule(Application app) {
        mApp = app;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(mApp);
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApp.getApplicationContext();
    }

    @Provides
    @Singleton
    SchedulersProvider provideSchedulers() {
        return new RealSchedulersProvider();
    }

    @Provides
    ExifReader provideExifReader() {
        return new ExifReader();
    }
}

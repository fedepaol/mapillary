package com.mapillary.mapillarychallenge.inject;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.mapillary.mapillarychallenge.ChallengeApplication;
import com.mapillary.mapillarychallenge.internals.ExifReader;
import com.mapillary.mapillarychallenge.internals.GpxProcessor;
import com.mapillary.mapillarychallenge.internals.PicturesFinder;
import com.mapillary.mapillarychallenge.schedule.SchedulersProvider;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(ChallengeApplication app);
    SharedPreferences getSharedPrefs();
    Context getContext();
    SchedulersProvider getSchedulers();
    ExifReader getExifReader();
    GpxProcessor getGpxProcessor();
    PicturesFinder getPicturesFinder();
}



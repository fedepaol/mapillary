package com.mapillary.mapillarychallenge;

import com.mapillary.mapillarychallenge.internals.FetchDataTask;
import com.mapillary.mapillarychallenge.internals.GpxProcessor;
import com.mapillary.mapillarychallenge.internals.PicturesFinder;
import com.mapillary.mapillarychallenge.model.GeoImage;
import com.mapillary.mapillarychallenge.model.GpxPoint;
import com.mapillary.mapillarychallenge.model.Image;
import com.mapillary.mapillarychallenge.model.Position;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by fedepaol on 29/03/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class FetchDataTest {
    FetchDataTask task;

    @Mock
    PicturesFinder picturesFinder;
    @Mock
    GpxProcessor gpxProcessor;

    List<GpxPoint> points;
    List<Image> images;

    @Before
    public void init() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        points = new LinkedList<>();
        images = new LinkedList<>();
        try {
            // these two should be dropped since they are not in between any two points
            images.add(Image.create("todropimage1", sdf.parse("2017-03-17T12:15:15.000Z")));
            images.add(Image.create("todropimage2", sdf.parse("2017-03-17T12:15:16.000Z")));


            points.add(GpxPoint.create(Position.create(55.60909717, 13.00419556), 4.0f, sdf.parse("2017-03-17T12:15:17.000Z")));
            points.add(GpxPoint.create(Position.create(55.60910385, 13.00421879), 4.0f, sdf.parse("2017-03-17T12:15:18.000Z")));

            // First Image
            images.add(Image.create("firstimagepath", sdf.parse("2017-03-17T12:15:18.000Z")));

            points.add(GpxPoint.create(Position.create(55.60911061, 13.00424499), 4.0f, sdf.parse("2017-03-17T12:15:19.000Z")));
            points.add(GpxPoint.create(Position.create(55.6091247, 13.00429275), 4.0f, sdf.parse("2017-03-17T12:15:20.000Z")));

            // Second Image
            images.add(Image.create("secondimagepath", sdf.parse("2017-03-17T12:15:20.990Z")));
            images.add(Image.create("thirdimagepath", sdf.parse("2017-03-17T12:15:20.995Z")));

            points.add(GpxPoint.create(Position.create(55.60913064, 13.00431644), 3.0f, sdf.parse("2017-03-17T12:15:21.000Z")));

            images.add(Image.create("todropimage3", sdf.parse("2017-03-17T12:15:23.000Z")));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        when(gpxProcessor.gpxParserObservable("BAR")).thenReturn(Observable.from(points));
        when(picturesFinder.imageDataObservable("FOO")).thenReturn(Observable.just(images));

        task = new FetchDataTask("FOO", "BAR", picturesFinder, gpxProcessor);
    }

    @Test
    public void testEmitsPoints() {
        TestSubscriber<GpxPoint> pointsSubscriber = new TestSubscriber();
        task.pointsObservable().subscribe(pointsSubscriber);
        assertEquals(0, pointsSubscriber.getOnNextEvents().size());
        task.startEmitting();
        List<GpxPoint> res = pointsSubscriber.getOnNextEvents();
        assertEquals(5, res.size());
        GpxPoint point = res.get(3);
        assertEquals(point.position().latitude(), 55.6091247, 0.00000001);
        assertEquals(point.position().longitude(), 13.00429275, 0.00000001);

    }

    @Test
    public void testEmitImages() {
        TestSubscriber<List<GeoImage>> imagesSubscriber = new TestSubscriber();
        task.geoImageObservable().subscribe(imagesSubscriber);
        assertEquals(0, imagesSubscriber.getOnNextEvents().size());
        task.startEmitting();
        List<List<GeoImage>> res = imagesSubscriber.getOnNextEvents();
        assertEquals(1, res.size());
        List<GeoImage> images = res.get(0);
        assertEquals(3, images.size());

        GeoImage image1 = images.get(0);
        assertEquals(image1.position().latitude(), 55.60910385, 0.00000001);
        assertEquals(image1.position().longitude(), 13.00421879, 0.00000001);
        GeoImage image2 = images.get(1);
        assertEquals(image2.position().latitude(), 55.60913064, 0.00000001);
        assertEquals(image2.position().longitude(), 13.00431644, 0.00000001);
    }
}

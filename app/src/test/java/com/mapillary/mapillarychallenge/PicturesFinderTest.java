package com.mapillary.mapillarychallenge;

import com.mapillary.mapillarychallenge.internals.ExifReader;
import com.mapillary.mapillarychallenge.internals.PicturesFinder;
import com.mapillary.mapillarychallenge.model.Image;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PicturesFinderTest {
    private PicturesFinder finder;
    private String path;
    String image1Path, image2Path, image3Path;
    Date date1, date2, date3;

    @Mock
    ExifReader exifReader;

    @Before
    public void init() {
        finder = new PicturesFinder(exifReader);
        path = getClass().getResource("images").getPath();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
        try {
            date3 = sdf.parse("17/03/2017-12:15:20");
            date1 = sdf.parse("17/03/2017-12:15:21");
            date2 = sdf.parse("17/03/2017-12:15:22");

            image1Path = getClass().getResource("images/aaa.jpeg").getPath();
            image2Path = getClass().getResource("images/bbbb.jpeg").getPath();
            image3Path = getClass().getResource("images/ccc.JPG").getPath();

            when(exifReader.getDateFromImage(image1Path)).thenReturn(date1);
            when(exifReader.getDateFromImage(image2Path)).thenReturn(date2);
            when(exifReader.getDateFromImage(image3Path)).thenReturn(date3);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void testPictureFinder() throws Exception {
        Observable<List<Image>> data = finder.imageDataObservable(path);
        TestSubscriber<List<Image>> subscriber = new TestSubscriber<>();
        data.subscribeOn(Schedulers.immediate()).subscribe(subscriber);

        List<List<Image>> res = subscriber.getOnNextEvents();
        int completions = subscriber.getCompletions();
        List<Throwable> errors = subscriber.getOnErrorEvents();
        assertEquals(1, completions);
        assertEquals(1, res.size());

        List<Image> images = res.get(0);
        assertEquals(3, images.size());
        assertEquals(image3Path, images.get(0).imagePath());
        assertEquals(date3, images.get(0).imageDate());

        assertEquals(image1Path, images.get(1).imagePath());
        assertEquals(date1, images.get(1).imageDate());

        assertEquals(image2Path, images.get(2).imagePath());
        assertEquals(date2, images.get(2).imageDate());

    }
}
package com.mapillary.mapillarychallenge;

import com.mapillary.mapillarychallenge.internals.GpxProcessor;
import com.mapillary.mapillarychallenge.model.GpxPoint;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class ProcessGpxTest {
    private GpxProcessor processor;
    private String fileName;

    public ProcessGpxTest() {
        fileName = getClass().getResource("sample.gpx").getPath();
        //inputStream = getClass().getResourceAsStream("sample.gpx");
        processor = new GpxProcessor();
    }

    @Test
    public void testFileOpener() throws Exception {
        Observable<GpxPoint> observable = processor.gpxParserObservable(fileName);
        TestSubscriber<GpxPoint> subscriber = new TestSubscriber<>();

        observable.subscribe(subscriber);
        List<GpxPoint> points = subscriber.getOnNextEvents();
        assertEquals(6, points.size());
        List<Throwable> errors = subscriber.getOnErrorEvents();
        assertEquals(0, errors.size());

        GpxPoint point = points.get(3);
        assertEquals(55.60911784, point.position().latitude(), 0.000001);
        assertEquals(13.00426896, point.position().longitude(), 0.000001);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
        Date d = sdf.parse("17/03/2017-12:15:20");
        assertEquals(d, point.date());


    }
}